import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Loader from './mainComponents/Loader';
import FreecodApp from './FreecodApp';

const Home = React.lazy( () => import('./views/pages/Home'));
const Login = React.lazy( () => import('./views/pages/Login'));
const Register = React.lazy( () => import('./views/pages/Register'));
const Advisor = React.lazy(() => import('./views/advisorView/Advisors'));

const MainViewApp = () =>{
    return(
        <BrowserRouter>
            <React.Suspense
                fallback = {<Loader />}
            >
                <Switch>

                    <Route 
                        path = '/'
                        exact
                        render = {props => <FreecodApp {...props} /> }
                    />
                    <Route 
                        path = '/home'
                        render = {props => <Home {...props} /> }
                    />
                    <Route 
                        path = '/login'
                        render = {props => <Login {...props} /> }
                    />
                    <Route 
                        path = '/register'
                        render = {props => <Register {...props} /> }
                    />
                    <Route 
                        path = '/advisors'
                        render = {props => <Advisor {...props} /> }
                    />
                    

                </Switch>
            </React.Suspense>
        </BrowserRouter>
    )
}

export default MainViewApp;