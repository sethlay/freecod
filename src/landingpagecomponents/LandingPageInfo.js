import React from 'react';
import { Card } from 'reactstrap';
import '../landingpagestyles/LandingPageInfo.css';
import Marquee from 'react-smooth-marquee';

const LandingPageInfo = () =>{
    return(
        <>
            <div
                className='landingInfoFirCon'
            >
                <Card
                    className='landingpageCardPicOne'
                >
                    <img
                        className='landingpageHand' 
                        src={require('./images/hands.jpg')} 
                    />
                </Card>
                <Card
                    className='landingpageCardYoutube'
                >
                    <iframe                         
                        className='iFrame' 
                        src="https://www.youtube.com/embed/KjXR4o2EFYI?autoplay=1" 
                        frameBorder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                        >

                    </iframe>
                </Card>
                <Card
                    className='landingpageCardPicCovid'
                >
                    <img
                        className='landingpageCovid' 
                        src={require('./images/covid.jpg')} 
                    />
                </Card>
            </div>
            <div
                className='landingInfoSecCon'
            >
                <Card
                    className='landingpageCardPicWorldStats'
                >
                    <img
                        className='landingpageWorldStats' 
                        src={require('./images/worldStats.jpg')} 
                    />
                </Card>
                <Card
                    className='landingpageCardPicPhFlag'
                >
                    <img
                        className='landingpagePhFlag' 
                        src={require('./images/phFlag.jpg')} 
                    />
                </Card>
            </div>
            
                <Marquee
                    velocity = {0.05}
                    className = 'Marquee'
                >
                    <div>
                    ABS-CBN News || Posted at Apr 18 2020 04:20 PM || MANILA (UPDATE)--The Philippines on Saturday breached the 6,000-mark in coronavirus disease 2019 cases after tallying 209 more patients, by DOH. 
                    </div>
                </Marquee>
            
            

        </>
    )
}

export default LandingPageInfo;