import React from 'react';
import '../landingpagestyles/LandingTextAccess.css';
import {Link} from 'react-router-dom';

const LandingTextAccess = props => {

    // const handleAccessButton = () =>{
        
    //     window.location.replace('/login');
    // }

    return(
        <>
            <h2
                className='textAccessFont'
            >Our mission is to educate people with the happenings on our surroundings during this current situation. Optimism and kindness can end every obstacle this world encounters. </h2>
            <h2
                className='textAccessNextFont'
            >Nonetheless, let us keep going together. </h2>
            <h2
                className='textAccessLastFont'
            >
                And together, we will survive.
            </h2>
            <button
                // type='button'
                className='landingButton'
                
            >
                <Link to='/login' className = 'exploreBtn' >Explore</Link>
            </button>
        </>
    )
}

export default LandingTextAccess;