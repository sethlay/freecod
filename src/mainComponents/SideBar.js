import React, { useState } from 'react';
import Sidebar from 'react-sidebar';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../mainStyles/sidebar.css';
import LocalLibraryOutlinedIcon from '@material-ui/icons/LocalLibraryOutlined';
import HouseOutlinedIcon from '@material-ui/icons/HouseOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';

const SideBar = props => {
    
    const [sidebarOpen, setSideBarOpen] = useState(false);
    
    const handleSideBarOpen = () => {
        setSideBarOpen(!sidebarOpen);
    } 

    return(
        <Sidebar
            open = {sidebarOpen}
            onSetOpen = {handleSideBarOpen}
            sidebarClassName = 'sideBarStyle'
            children = {<> </>}
            // styles = {{ sidebar: {background: 'white'} }}
            sidebar = {
                <div
                    className = 'sideBarContainer'
                >
                    <ListGroup>
                        <Link
                            to = '/home'
                            className = 'linkHomeStyle'
                        >
                            
                            <HouseOutlinedIcon 
                                className = 'iconHomeStyle'
                            />   
                            
                        </Link>
                    </ListGroup>
                    <ListGroup>
                        <Link
                            to = '/advisors'
                            className = 'linkBudStyle'
                        >
                            <PeopleAltOutlinedIcon 
                                className = 'iconBudStyle'
                            />   
                        </Link>
                    </ListGroup>
                    <ListGroup>
                        <Link
                            to = '/'
                            className = 'linkLogOut'
                        >
                                <ExitToAppOutlinedIcon 
                                    className = 'iconOutStyle'
                                />   
                        </Link>
                    </ListGroup>
                </div>
            }
        >
            <button
                className = 'sideBarBtn'                    
                onClick = { () => setSideBarOpen(!sidebarOpen) }
            >
                {/* <i className="material-icons">person</i> */}
                <LocalLibraryOutlinedIcon 
                    className = 'mainIconBtn'
                />
                
            </button>
        </Sidebar>
    )
}   



export default SideBar;