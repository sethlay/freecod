import React from 'react';
import { toast } from 'react-toastify';


const UnRegisteredToast = (message) => {
    toast.error(message);
}

const ErrorLoginToast = (message) => {
    toast.success(message)
}

const SuccessToast = (message) => {
    toast.success(message)
}

const ErrorToast = (message) => {
    toast.success(message)
}

const SuccesPayToast = (message) => {
    toast.success(message)
}

const ErrorPayToast = (message) => {
    toast.success(message)
}

export {
    UnRegisteredToast,
    ErrorLoginToast,
    SuccessToast,
    ErrorToast,
    SuccesPayToast,
    ErrorPayToast
}