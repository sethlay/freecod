import React, { useState } from 'react';
import '../../mainStyles/login.css';
import {Button} from 'reactstrap';
import {Link,useHistory} from 'react-router-dom';
import { ErrorLoginToast } from '../../mainComponents/Toasts';
import 'react-toastify/dist/ReactToastify.min.css';
// import '../../mainComponents/Toasts.css';

const Login = props => {
    
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const {push} = useHistory()

    const handleLogin = async () => {
        try{
            // console.log(handleLogin)
        const apiOptions = {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                email,
                password
            })
        }

        const fetchedData = await fetch('https://quiet-reaches-06570.herokuapp.com/login', apiOptions);
        
        const data = await fetchedData.json()

        sessionStorage.token = data.token;
        sessionStorage.userId = data.user.id;
        sessionStorage.isAdmin = data.user.isAdmin;
        sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
        sessionStorage.email = data.user.email;

        // window.location.replace('/home');

        push('/home');
        
        }catch{
            ErrorLoginToast('Unauthorized account. Kindly register.')
        }
    }

    // const handleRegister = () => {

    //     window.location.replace('/register')

    // }


    return(
        <div
            className='containerMain'
        >
            <div
                className = 'subContainer'
            >
                <h2
                    className='loginFont'
                >Too Curious?</h2>
                <form>
                    <div
                        className = 'emailForm'
                    >
                        <label
                            className = 'emailLabel'
                        >Email:</label>
                        <input 
                            className = 'emailInput'
                            type = 'email'
                            placeholder = 'Enter your email'
                            onChange = {(e) => setEmail(e.target.value)}
                        />
                    </div>
                    <div
                        className = 'passwordForm'
                    >
                        <label>Password:</label>
                        <input
                            className = 'passwordInput' 
                            type = 'password'
                            placeholder = 'Enter your password'
                            onChange = {(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <Button
                        className = 'loginBtn'
                        disabled = {email === '' || password === '' ? true : false}
                        onClick = {handleLogin}
                        
                    >Login</Button>
                    
                    <hr
                        className = 'hrStyle'
                    />
                    <Button
                        className = 'registerBtn'
                        
                    >
                        <Link to='/register'>Register here</Link>
                    </Button>
                </form>
            </div>
            
        </div>
    )
}

export default Login;