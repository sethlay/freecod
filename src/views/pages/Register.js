import React, { useState } from 'react';
import '../../mainStyles/register.css';
import { Button } from 'reactstrap';
import {useHistory} from 'react-router-dom';
// import { UnRegisteredToast } from '../../mainComponents/Toasts';

const Register = props => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState ('');
    const [confirmPassword, setConfirmPassword] = useState('')

    const {push} = useHistory()

    const handleRegister = async () => {
        const apiOptions = {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        }

        let fetchedData = await fetch('https://quiet-reaches-06570.herokuapp.com/register', apiOptions);
        
        // window.location.replace('/login');
        push('/login');

        // if(fetchedData.ok) {
            
        //     window.location.replace('/login');

        // }else{
        //     UnRegisteredToast('An error occured. Please try again.')
        // }
        
    }

    return(
        <div
            className='containerMain'
        >
            <div
                className = 'subContainer'
            >
                <h2
                    className='loginFont'
                >Take Courage and Join Us!</h2>
                <hr 
                    className = 'hrStyle'
                />
                <form>
                    <div
                        className = 'nameForm'
                    >
                        <label>Firstname:</label>
                        <input 
                            className = 'nameInput'
                            type = 'text'
                            placeholder = 'Enter your first name'
                            onChange = {(e) => setFirstName(e.target.value)}
                        />
                    </div>
                    <div
                        className = 'nameForm'
                    >
                        <label>Lastname:</label>
                        <input 
                            className = 'nameInput'
                            type = 'text'
                            placeholder = 'Enter your last name'
                            onChange = {(e) => setLastName(e.target.value)}
                        />
                    </div>
                    <div
                        className = 'emailForm'
                    >
                        <label
                            className = 'emailLabel'
                        >Email:</label>
                        <input 
                            className = 'emailInput'
                            type = 'email'
                            placeholder = 'Enter your email'
                            onChange = {(e) => setEmail(e.target.value)}
                        />
                    </div>
                    <div
                        className = 'passwordForm'
                    >
                        <label>Password:</label>
                        <input
                            className = 'passwordInput' 
                            type = 'password'
                            placeholder = 'Enter your password'
                            onChange = {(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div
                        className = 'confirmPasswordForm'
                    >
                        <label>Confirm Password:</label>
                        <input
                            className = 'confirmPasswordInput' 
                            type = 'password'
                            placeholder = 'Enter your password'
                            onChange = {(e) => setConfirmPassword(e.target.value)}
                        />
                        <span
                            className = 'spanFont'
                        >
                            {confirmPassword !== '' && confirmPassword !== password ? 'Password did not match' : ''}
                        </span>
                    </div>
                    <Button
                        className = 'loginBtn'
                        type = 'button'
                        disabled = {firstName === '' || lastName === '' || email === '' || password === '' || password !== confirmPassword ? true : false}
                        onClick = {handleRegister}
                    >Register</Button>
                </form>
            </div>
        </div>
    )
}

export default Register;