import React, { useEffect } from 'react';
import SideBar from '../../mainComponents/SideBar';
import './homeComponentsStyles/homeStyles.css';
import {useHistory} from 'react-router-dom';

const Home = props => {

    const {push} = useHistory()

    useEffect(() => {
        if(!sessionStorage.token) {
            // window.location.replace('/login')
            push("/login")
        }
    })

    const handleConsultButton = () => {
        // window.location.replace('/advisors');
        push("/advisors")
    }

    return(
        <div className = 'containerMain' >
            <div
                className = 'containerSub'
            >
                <SideBar 
                    style = {{
                        position: 'absolute'
                    }}
                />
                <div>
                <div
                    className = 'containerSubContent'
                >
                    <img                    
                        src = {require('./homeComponentsStyles/homeImages/imgFour.jpg')}
                        // data-aos="fade-right"
                        
                        className = 'imgFirst'
                    />
                    <img                    
                        src = {require('./homeComponentsStyles/homeImages/imgOne.jpg')}
                        // data-aos="fade-left"
                        className = 'imgSec'
                        
                    />
                    <img                    
                        src = {require('../../landingpagecomponents/images/docOne.jpg')}
                        // data-aos="fade-right"
                        className = 'imgThi'
                        
                    />
                    <img                    
                        src = {require('../../landingpagecomponents/images/hands.jpg')}
                        // data-aos="zoom-in"
                        className = 'imgFou'
                        
                    />
                    
                </div>
                <div
                    className = 'containerInfo'
                >
                    <p
                        className = 'infoFirstPar'
                    >FreeCOD, the Coronavirus (COVID-19) resources for you and your loved ones.</p>
                    
                    <p className = 'infoSecP'>
                    Worried about coronavirus? </p>
                    
                    <p className = 'infoFirstP'>
                    We can help you a list of important resources to access during the COVID-19 pandemic. Find out how the virus is progressing and how you can protect yourself and loved ones with the most up to date news, developments and best practices.
                    </p>
                    <p
                        className = 'infoFirstP'
                    >
                    Traditional consultation takes up so much time just getting to your appointment. Skip the waiting room! Instantly connect with councilors for medical care! Reach a Councilor anytime of the day, from the comfort of your own home. Our network of councilors is available on weekdays. Book the help you need safely, when you need it.
                    </p>
                    <p
                        className = 'infoFirstP'
                    >
                    Whether you’re looking for a doctor’s visit for yourself, or for a backup plan for your family, we’ve got you covered at FreeCOD.
                    </p>
                    <p
                        className = 'infoFirstP'
                    >
                    FreeCOD makes it easy to BOOK your Councilor and access appointments, so you can get the care you need right away.
                    </p>
                </div>
                <div>
                    <a
                        href = 'https://l.facebook.com/l.php?u=https%3A%2F%2Finvite.viber.com%2F%3Fg2%3DAQBGKGCV%2BtT7H0tB05FjEScE8F8zFOdEN0ipp7qvC2BE%2F%2Bk8rgFHurdBzkplMjj2%26fbclid%3DIwAR2OLHVsXM08Nn4Mm1D_VDNXcqls0n0acQatqJ0G7rSPX2svMvmFaSuRx3w&h=AT1VoMrP4nYua98YAmkMjcvZfe3doSU0RljpDizvijTWLlzaycEvAMW9N7vSM3LQbpNKB9C8SiCf0R0J_EwXE0cb2KUh2F0uAnCBx3NUuGsPKG__Wv8oZWN01pTQW8DKCg'
                    >
                        <img 
                        className = 'viberIcon'
                        src = {require('./homeComponentsStyles/homeImages/viber.png')}
                        
                    />
                    </a>
                    <a
                        href = 'https://m.facebook.com/1999040173718294'
                    >
                        <img 
                        className = 'polahIcon'
                        src = {require('./homeComponentsStyles/homeImages/polahFB.JPG')}
                        
                    />
                    </a>
                    <a
                        href = 'https://m.facebook.com/110702617243645'
                    >
                        <img 
                        className = 'maraIcon'
                        src = {require('./homeComponentsStyles/homeImages/maraFB.JPG')}
                        
                    />
                    </a>
                    <a
                        href = 'https://www.metromart.com/'
                    >
                        <img 
                        className = 'metroIcon'
                        src = {require('./homeComponentsStyles/homeImages/metroOne.png')}
                        
                    />
                    </a>
                    <a
                        href = 'https://www.who.int/westernpacific/emergencies/covid-19/news-2019-ncov'
                    >
                        <img 
                        className = 'whoIcon'
                        src = {require('./homeComponentsStyles/homeImages/who.JPG')}
                        
                    />
                    </a>
                    <a
                        href = 'https://fightcovid.app/index.html'
                    >
                        <img 
                        className = 'checkIcon'
                        src = {require('./homeComponentsStyles/homeImages/checkSelf.JPG')}
                        
                    />
                    </a>
                    <a
                        href = 'https://www.doh.gov.ph/'
                    >
                        <img 
                        className = 'dohIcon'
                        src = {require('./homeComponentsStyles/homeImages/doh.png')}
                        
                    />
                    </a>
                    <a
                        href = 'https://www.konsulta.md/?fbclid=IwAR1lqsT_icGSsrvei6cqjExuuiAUESldkJz04gFST5Vjikbq8ynpVutaU9Y'
                    >
                        <img 
                        className = 'konsultaIcon'
                        src = {require('./homeComponentsStyles/homeImages/konsulta.png')}
                        
                    />
                    </a>
                    <button
                        onClick = {handleConsultButton}
                        className = 'bookConsult'
                    >
                        Something to inquire? Book an appointment with us.
                    </button>
                    <p
                        className = 'partNers'
                    >Other participating partners to explore:</p>
                </div>
            </div>
        </div>
            
            
            
        </div>
    )
}

export default Home;