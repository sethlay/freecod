import React from 'react';
import SideBar from '../../mainComponents/SideBar';
import './advisorStyles/Advisors.css';
import Hrs from './advisorcomponents/Hrs';
import Locations from './advisorcomponents/locations/Locations';
import ListAdvisors from './advisorcomponents/listAdvisors/ListAdvisors';
import MyBooking from './advisorcomponents/mybookings/MyBookings';

const Advisors = () => {
    return(
        <>
            
            <div
                className = 'advisorPageStyle'
            >
                <SideBar />
                <div
                    className = 'leftContainer'
                >
                    <div
                        className = 'leftLocationContainer'
                    >

                        <div
                          className="divScrollContainer"
                        >
                        <Locations />
                        </div>
                    </div>
                    <div
                        className = 'leftListAdvisorContainer'
                    >
                        <div
                            className="divScrollContainer"
                        >
                            <ListAdvisors />
                        </div>
                        
                    </div>
                </div>
                <div
                    className = 'rightContainer'
                >
                    <h5
                        className = 'bookingTitleh5'
                    >Could it be nice if we do it together?</h5>
                    <h1
                        className = 'bookingTitleh1'
                    >FREECOD CONSULTATION</h1>
                    <Hrs />
                    <div
                        className = 'myBookingContainer'
                    >
                        <div
                            className = 'myBookingSubContainer'
                        >
                            <MyBooking />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Advisors;