import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, Card, CardHeader, CardImg, Label, CardBody, CardTitle, CardSubtitle, FormGroup, Input, Button } from 'reactstrap';
import ReactDayPicker from './ReactDayPicker';
import { formatMs } from '@material-ui/core';
import { SuccessToast } from '../../../../mainComponents/Toasts';


const ScheduleForm = ({showScheduleForm, toggleScheduleForm, advisorDetail}) => {
   

    const [schedules, setSchedules] = useState([]);
    const [advisors, setAdvisors] = useState([]);
    const [locations, setLocations] = useState([]);
    const [blockedDates, setBlockedDates] = useState([]);

    // files we need
    const [advisor, setAdvisor] = useState(null);
    const [user, setUser] = useState(null);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [amount, setAmount] = useState(0);
    const [dateRange, setDateRange] = useState({ from: null, to: null, enteredTo: null });

    
    useEffect(() => {
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/advisors')
        .then(res => res.json())
        .then(res => {
            
            setAdvisors(res)
        })

    }, [])

    
    const handleSaveBooking = () => {

        // console.log(advisorDetail)

        const {from, to} = dateRange;

        const dateDiff = (to.getDate() - from.getDate()) + 1;
              
        const amount = advisorDetail.price * dateDiff;


        const apiOptions = {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                advisor: advisorDetail._id,
                user: sessionStorage.userId,
                startDate: from,
                endDate: to,
                amount,
                payment: 'Pending'
            })
        }

        fetch('https://quiet-reaches-06570.herokuapp.com/admin/addschedule', apiOptions)
        .then(res => res.json())
        .then(res => {
            res.advisor = advisor;
            res.user = user;

            SuccessToast('Successfully booked. Pay now.')

            const newSchedules = [res, ...schedules];
            setSchedules(newSchedules);
        })

        toggleScheduleForm();
        // setBlockedDates(dates)
        // console.log(handleSaveBooking)
    }

    
    // const handleSaveBooking = (advisorDetail) => {
        
    //     const dates = [];
    //     props.schedules.forEach(indivSchedule => {
    //         if(indivSchedule.advisorDetail._id === advisorDetail._id) {
    //             dates.push({start: indivSchedule.startDate, end: indivSchedule.endDate})
    //         }
    //     })
    //     setBlockedDates(dates); 
    // }

    // const handleChooseDates = (day) => {
    //     const range = DateUtils.addDayToRange(day, dateRange);
    //     setDateRange(range);
    // }

    

    return(
        <>          

            <Modal
                isOpen = {showScheduleForm}
                toggle = {toggleScheduleForm}
            >
                <ModalHeader
                    toggle = {toggleScheduleForm}
                    style = {{
                        color: 'midnightblue',
                        fontWeight: 'bolder',
                        textTransform: 'uppercase'
                    }}
                >
                    Book & Consult
                </ModalHeader>
                <ModalBody>
                    
                        <Card
                            key = {advisorDetail._id}
                        >
                            <CardHeader
                                style = {{
                                    color: 'midnightblue',
                                    fontWeight: 'bolder',
                                    textTransform: 'uppercase',
                                    padding: 0
                                }}
                            >  
                                <CardImg 
                                src = {'https://quiet-reaches-06570.herokuapp.com/' + advisorDetail.coverImg}
                                style = {{
                                    height: '25%',
                                    width: '25%'
                                }}
                            />{advisorDetail.name}
                            </CardHeader>
                            <CardBody
                                className = 'text-center'
                            >
                                <CardTitle
                                    style = {{
                                        color: 'midnightblue',
                                        margin: '1px'
                                    }}
                                >Specialization: {advisorDetail.credentials} </CardTitle>
                                <CardSubtitle
                                    style = {{
                                        color: 'midnightblue',
                                        margin: '1px'
                                    }}
                                >
                                    Contact: {advisorDetail.contact}
                                </CardSubtitle>
                                <CardSubtitle
                                    style = {{
                                        color: 'midnightblue',
                                        margin: '1px'
                                    }}
                                >
                                    Location: {advisorDetail.location?.location}
                                </CardSubtitle>
                                <CardSubtitle
                                    style = {{
                                        color: 'midnightblue',
                                        margin: '1px'
                                    }}
                                >
                                    Price: Php {advisorDetail.price}
                                </CardSubtitle>
                                <FormGroup>
                                    <Label
                                        className = 'm-3'
                                        style = {{
                                            textDecoration: 'underline',
                                            fontWeight: 'bold',
                                            color: 'darkred'
                                        }}
                                    >Available Dates:</Label>
                                    <ReactDayPicker
                                        dateRange = {dateRange} 
                                        setDateRange = {setDateRange}
                                        advisorDetail = {advisorDetail} 
                                    />
                                </FormGroup>
                                
                                <Button
                                    onClick = {handleSaveBooking}
                                >Book</Button>
                            </CardBody>
                        </Card>
                            
                </ModalBody>
            </Modal>
        </>
    )
}

export default ScheduleForm;
