import React, { useState, useEffect } from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import MomentLocalUtils, {formatDate, parseDate} from 'react-day-picker/moment';

const ReactDayPicker = ({dateRange, setDateRange, advisorDetail}) => {


    const initialState = { from: null, to: null, enteredTo: null }
    const [blockedDates, setBlockedDates] = useState([]);
    const [schedules, setSchedules]=useState([]);
    const [pikachuArray, setPikachuArray] = useState([]);
    

    useEffect(()=>{
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/pogi/'+advisorDetail._id)
        .then(res => res.json())
        .then(res => {
           const newSchedules = res.map(({startDate,endDate})=>{
              return {from:new Date(startDate),to:new Date(endDate)}
           })
           newSchedules.push({before: new Date()})
           newSchedules.push({daysOfWeek: [0, 6]})
           
           setSchedules(newSchedules);

           const pikachu = [];
           
           res.forEach(({startDate, endDate}) => {
                let from = new Date(startDate);

                while(+from <= +new Date(endDate)){
                    pikachu.push(+from)
                    from.setDate(from.getDate() + 1)
                    
                }
                // +1 to increment the day
           })
           pikachu.forEach((e) => console.log(new Date(e)))
        //    console.log(pikachu)

           setPikachuArray(pikachu)

        })
    },[])



    const isSelectingFirstDay = (from, to, day) => {
        const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
        const isRangeSelected = from && to;

        return !from ||isBeforeFirstDay || isRangeSelected;
    }

    const handleDayClick = (day, {disabled}) => {
        // console.log(pikachu)
        if(disabled) return;

        const {from, to} = dateRange;
        if(from && to && day >= from && day <=to){
            handleResetClick()
            return;
        }
        if(isSelectingFirstDay(from, to, day)) {
            setDateRange({
                from: day,
                to: null,
                enteredTo: null
            })
        }else {

            const startDate = from.toString();
            const pikachu = pikachuArray

            let isAvailable = true;
      
            while(+from <= +day){
                if(pikachu.includes(+from)||from.getDay()===0||from.getDay()===6) isAvailable=false
                from.setDate(from.getDate()+1)
            }

            if(!isAvailable){
                return setDateRange(initialState)
            }

            setDateRange({
                from:new Date(startDate),
                to: day,
                enteredTo: day
            })
        }
    }

    const handleDayMouseEnter = (day) => {
        const {from, to} = dateRange;
        if(!isSelectingFirstDay(from, to, day)){
            setDateRange({
                from,
                to,
                enteredTo: day
            })
        }
    }

    const handleResetClick = () => {
        setDateRange(initialState);
    }

    const {from, enteredTo} = dateRange;



    // blockedDates.forEach(indivDates => {
    //     disabledDays.push({
    //         after: new Date(indivDates.start),
    //         before: new Date(indivDates.end)
    //     })
    // })


    return(
        <>
            
            <DayPicker 
                className = 'Range'
                numberOfMonths = {1}
                fromMonth = {from}
                selectedDays = {[from, {from, to: enteredTo}]}
                disabledDays = { schedules}
                modifiers = {{ start: from, end: enteredTo }}
                onDayClick = {handleDayClick}
                onDayMouseEnter = {handleDayMouseEnter}
            />
            <div> 
                {!dateRange.from && !dateRange.to && 'Please select the first day.'}
                {dateRange.from && !dateRange.to && 'Please select the last day.'}
                {dateRange.from && 
                    dateRange.to && 
                        `${dateRange.from.toLocaleDateString()} to ${dateRange.to.toLocaleDateString()}`}{' '} {dateRange.from && dateRange.to && ( 
                        
                    <button className="link" onClick={handleResetClick}> Reset </button> 
                )} 
            </div>
        </>
    )
}

export default ReactDayPicker;