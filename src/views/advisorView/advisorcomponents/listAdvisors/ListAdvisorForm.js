import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, Label, Button } from 'reactstrap';

const ListAdvisorForm = ({ showForm, handleSaveAdvisor, toggleShowForm, isEditing, advisorToEdit }) => {

    const [locations, setLocations] = useState([]);
    // console.log(locations)
    const [isOpenLocation, setIsOpenLocation] = useState(false)

    const [location, setLocation] = useState(null);
    // console.log(location)
    const [name, setName] = useState('');
    const [contact, setContact] = useState(0);
    const [credentials, setCredentials] = useState('');
    const [coverImg, setCoverImg] = useState(null);
    const [price, setPrice] = useState(0);

    useEffect(() => {
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/locations')
        .then(res => res.json())
        .then(res => { 
            // console.log(res)
            setLocations(res) 
        })
    }, [])

    const selectMainImage = async(e) => {
        
        let coverImg = e.target.files[0];
        
        if(coverImg.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/)) {
            setCoverImg(coverImg);
         
        }
    }

    const uploadMainImage = async () => {
        const data = new FormData();
        data.append('image', coverImg, coverImg.name)
    
        const coverImgData = await fetch('https://quiet-reaches-06570.herokuapp.com/upload', {
            method: 'POST',
            body: data
        })

        const coverImgUrl = await coverImgData.json();

        console.log(coverImgUrl);

        return coverImgUrl;
    }

    const handleSave = async () => {

        const {imageUrl} = await uploadMainImage()

        handleSaveAdvisor(name, location, credentials, contact, imageUrl, price)

        setName('');
        setLocation(null);
        setCredentials('');
        setContact(0);
        setCoverImg('');
        setPrice(0);
    }


    return(
        <Modal
            className = 'advisorModalForm'
            isOpen = {showForm}
            toggle = {toggleShowForm}
        >
            <ModalHeader
                className = 'advisorModalHeader'
                toggle = {toggleShowForm}
            >
                {isEditing ? 'Edit Advisor' : 'Add Advisor'}
            </ModalHeader>
            <ModalBody>
                <FormGroup>
                    <Label>Name:</Label>
                    <Input
                        className = 'inputStyle' 
                        placeholder = 'Enter full name here'
                        type = 'text'
                        onChange = {(e) => setName(e.target.value)}
                        defaultValue = {isEditing ? advisorToEdit.name : ''}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Location:</Label>
                    <Dropdown
                        isOpen = {isOpenLocation}
                        toggle = {() => setIsOpenLocation(!isOpenLocation)}
                    >
                        <DropdownToggle 
                            caret
                            className = 'locationDropDown'
                        >
                            { isEditing && location === null 
                            ? advisorToEdit.location.location 
                            : !location 
                            ? 'Choose Location' 
                            : location.location}
                        </DropdownToggle>
                        <DropdownMenu>
                            {locations.map(indivLocation => (
                                <DropdownItem
                                    key = {indivLocation._id}
                                    onClick = {() => setLocation(indivLocation)}
                                >
                                    {indivLocation.location}
                                </DropdownItem>
                            ))}
                        </DropdownMenu>
                    </Dropdown>
                </FormGroup>
                <FormGroup>
                    <Label>Contact Number:</Label>
                    <Input
                        className = 'inputStyle' 
                        placeholder = 'Enter mobile number'
                        type = 'number'
                        onChange = {(e) => setContact(e.target.value)}
                        defaultValue = {isEditing ? advisorToEdit.contact : ''}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Credentials:</Label>
                    <Input 
                        className = 'inputStyle'
                        placeholder = 'Enter credentials'
                        type = 'text'
                        onChange = {(e) => setCredentials(e.target.value)}
                        defaultValue = {isEditing ? advisorToEdit.credentials : ''}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Consultation Fee:</Label>
                    <Input
                        className = 'inputStyle' 
                        placeholder = 'Enter Fixed Amount'
                        type = 'number'
                        onChange = {(e) => setPrice(e.target.value)}
                        defaultValue = {isEditing ? advisorToEdit.price : ''}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Image:</Label>
                    <Input
                        className = 'inputStyle' 
                        placeholder = 'Image'
                        type = 'file'
                        onChange = {selectMainImage}
                    />
                </FormGroup>
                <Button
                    className = 'listAdvisorBtn'
                    block
                    onClick = {handleSave}
                >
                    {isEditing ? 'Edit Advisor' : 'Add Advisor'}
                </Button>
            </ModalBody>
        </Modal>
    )
}

export default ListAdvisorForm;