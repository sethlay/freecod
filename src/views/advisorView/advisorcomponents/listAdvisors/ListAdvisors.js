import React, { useState, useEffect } from 'react';
import {Table} from 'reactstrap';
import './listAdvisorStyles/ListAdvisors.css';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import ListAdvisorForm from './ListAdvisorForm';
import ListAdvisorRow from './ListAdvisorRow';
import ScheduleForm from '../schedules/ScheduleForm';

const ListAdvisor = () => {

    const [advisors, setAdvisors] = useState([]);
    // console.log(advisors)
    const [showForm, setShowForm] = useState(false);
    const [showScheduleForm, setshowScheduleForm] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [advisorToEdit, setAdvisorToEdit] = useState({});
    const [advisorDetail, setAdvisorDetail] = useState({});

    useEffect(() => {
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/advisors')
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            setAdvisors(res);
        })
    }, [])

    const toggleShowForm = () =>{
        setShowForm(!showForm)
        setIsEditing(false)
    }    

    const toggleScheduleForm = () => {
        setshowScheduleForm(!showScheduleForm);
    }

    const handleSaveAdvisor = (name, location, credentials, contact, coverImg, price) => {
        
        console.log(coverImg)

        // updating/editing advisor 
        if (isEditing) {
            
            let editedname = name;
            let editedlocation = location;
            let editedcredentials = credentials;
            let editedcontact = contact;
            let editedcoverImg = coverImg;
            let editedprice = price;
            
            if(editedname === '') {
                editedname = advisorToEdit.name;
            }
            if(location === null) {
                editedlocation = advisorToEdit.location;
            }
            if(editedcredentials === '') {
                editedcredentials = advisorToEdit.credentials;
            }
            if(editedcontact === 0) {
                editedcontact = advisorToEdit.contact;
            }
            if(editedcoverImg === '') {
                editedcoverImg = advisorToEdit.coverImg;
            }
            if(editedprice === 0) {
                editedprice = advisorToEdit.price;
            }

            const apiOptions = {
                method: 'PATCH',
                headers: {'Content-type' : 'application/json'},
                body: JSON.stringify({
                    id: advisorToEdit._id,
                    name: editedname,
                    location: editedlocation._id,
                    credentials: editedcredentials,
                    contact: editedcontact,
                    coverImg: editedcoverImg,
                    price: editedprice
                }),
            }

            fetch('https://quiet-reaches-06570.herokuapp.com/admin/updateadvisor', apiOptions)
            .then(res=> res.json())
            .then(res => {
                let newAdvisors = advisors.map((advisor) => {
                    if(advisor._id === advisorToEdit._id) {
                        res.location = editedlocation
                        return res;
                    }
                    return advisor;
                })
                setAdvisors(newAdvisors);
                setIsEditing(false);
            })

        }else {

            // add advisor
            let apiOptions = {
                method: 'POST',
                headers: {'Content-type' : 'application/json'},
                body: JSON.stringify({
                    name,
                    location: location._id,
                    credentials,
                    contact,
                    coverImg,
                    price
                })
            }

            fetch('https://quiet-reaches-06570.herokuapp.com/admin/addadvisor', apiOptions)
            .then(res => res.json())
            .then(res => {
                res.location = location;
                const newAdvisors = [res, ...advisors];

                setAdvisors(newAdvisors)
            })
        }        

        toggleShowForm();
        setIsEditing(false);
    }

    const editAdvisor = (advisor) => {
        setShowForm(true);
        setIsEditing(true);
        setAdvisorToEdit(advisor);
        console.log(advisor)
    }

    const handleDeleteAdvisor = id => {
        
        const apiOptions = {
            method: 'DELETE',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({id})
        }

        fetch('https://quiet-reaches-06570.herokuapp.com/admin/deleteadvisor', apiOptions)
        .then(res => res.json())
        .then(res => {
            const newAdvisors = advisors.filter(advisor => {
                return advisor._id != id
            })
            setAdvisors(newAdvisors)
        })
    }

    const updateAvailable = advisor => {
        
        const apiOptions = {
            method: 'PATCH',
            headers: { 'Content-type' : 'application/json'},
            body: JSON.stringify({
                id: advisor._id,
                isAvailable: !advisor.isAvailable
            })
        }

        fetch('https://quiet-reaches-06570.herokuapp.com/admin/updateadvisor/isAvailable', apiOptions)
        .then(res => res.json())
        .then(res => {
            let newAdvisors = advisors.map(indivAdvisor => {
                if(indivAdvisor._id === advisor._id) {
                    res.location = indivAdvisor.location;
                    return res;
                }
                return indivAdvisor;
            });
            setAdvisors(newAdvisors);
        })

    }

    return(
        <>
            <Table>
                <thead>
                    <tr>
                        <th
                            className = 'listAdFontStyle'
                        >LIST OF OUR BEST CONSULTANTS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {advisors.map(advisor => (
                        <ListAdvisorRow 
                            key = {advisor._id}
                            advisor = {advisor}
                            handleDeleteAdvisor = {handleDeleteAdvisor}
                            editAdvisor = {editAdvisor}
                            updateAvailable ={updateAvailable}
                            toggleScheduleForm = {toggleScheduleForm}
                            setAdvisorDetail = {setAdvisorDetail}
                        />
                    ))}
                </tbody>
            </Table>
            <button
                className = 'addBtnAdvisor'
                onClick = {toggleShowForm}
            >
                <PersonAddIcon />
            </button>
            <ListAdvisorForm 
                showForm = {showForm}
                handleSaveAdvisor = {handleSaveAdvisor}
                toggleShowForm = {toggleShowForm}
                isEditing = {isEditing}
                advisorToEdit = {advisorToEdit}
                
            />
            <ScheduleForm 
                showScheduleForm = {showScheduleForm}
                toggleScheduleForm = {toggleScheduleForm}
                advisorDetail = {advisorDetail}
            />
        </>
    )
}

export default ListAdvisor;