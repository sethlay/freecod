import React, {useState} from 'react';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import EditIcon from '@material-ui/icons/Edit';
import CallIcon from '@material-ui/icons/Call';



const ListAdvisorRow = ({ advisor, handleDeleteAdvisor,editAdvisor, updateAvailable, toggleScheduleForm,setAdvisorDetail }) => {

    

    return(
        <>
            <tr>
                <td
                    className = 'advisorRowStyle'
                    onClick = {() => updateAvailable(advisor)}
                    style = {{
                        color: 'midnightblue'
                    }}
                >
                    {advisor.isAvailable ? (advisor.name) : 'Not Available'}
                </td>
                <td>
                <button
                    className = 'callIconStyle'
                
                    disabled = {advisor.isAvailable ? false : true}
                    onClick = {() => {
                        toggleScheduleForm()
                        setAdvisorDetail(advisor)
                    }}
                >
                    <CallIcon 
                        className = 'deleteForeveIcon'
                    />
                </button>
                <button
                    onClick = {()=>handleDeleteAdvisor(advisor._id)}
                    className = 'locationDelete'
                >
                    <PersonAddDisabledIcon 
                        className = 'deleteForeveIcon'
                    />
                    
                </button>
                <button
                    className = 'locationDelete'
                    onClick = {() => editAdvisor(advisor)}
                >
                    <EditIcon className = 'deleteForeveIcon' />
                </button>
                </td>
            </tr>

            

        </>
    )
}

export default ListAdvisorRow;