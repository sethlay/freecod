import React, { useState, useEffect } from 'react'
import AddLocationOutlinedIcon from '@material-ui/icons/AddLocationOutlined';
import LocationRow from './LocationRow';
import './locationStyles/Locations.css';
import { Table } from 'reactstrap';
import LocationForm from './LocationForm';

const Locations = props => {

    const [locations, setLocations] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [locationToEdit, setLocationToEdit] = useState({});
    const [idToEdit, setIdToEdit] = useState('');
    
    useEffect(() => {

        // all locations
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/locations')
        .then(res => res.json())
        .then(res => { setLocations(res) })
    }, [])
    
    const handleSaveLocation = (location) => {
        // console.log(location)
        let newLocations = [];  
        // edit location
        if (isEditing) {
            let editingLocation = location;

            if (location === '') editingLocation = locationToEdit.location;

            fetch('https://quiet-reaches-06570.herokuapp.com/admin/updatelocation', {
                method: 'PATCH',
                headers: { 'Content-type' : 'application/json'},
                body: JSON.stringify({
                    id: idToEdit,
                    location: editingLocation
                })
            })
            

            setLocationToEdit({});
            setIdToEdit('');
            setIsEditing(false);
        } else {

            // add to database
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/addlocation', {
            method: "POST",
            headers: { 'Content-Type' : 'application/json' },
            body: JSON.stringify({
                location: location
            })
        })
        .then(res => res.json())
        .then(res => { setLocations([res, ...locations]) })
 
        }

        setShowForm(!showForm);        
    }

    const toggleForm = () => {
        setShowForm(!showForm);
    }

    const handleDeleteLocation = (id) => {

        // console.log(id)

        const apiOptions = {
            method: 'DELETE',
            headers: { 'Content-type' : 'application/json' },
            body: JSON.stringify({ id })
        }
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/deletelocation',apiOptions)
        .then(res => res.json())
        .then(res => {
            const newLocations = locations.filter(location => {
                return location._id != id
            });
            setLocations(newLocations);
        })
    }

    return (
        <>
            
            <div>
                <Table>
                    <thead>
                        <tr>
                            <th
                                className = 'locationFontStyle'
                            >LOCATIONS</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {locations.map((location, index) => {
                        
                        return(
                            <LocationRow 
                                key = {index}
                                location = {location}
                                handleDeleteLocation = {handleDeleteLocation}
                                setShowForm = {setShowForm}
                                setIsEditing ={setIsEditing}
                                setLocationToEdit = {setLocationToEdit}
                                setIdToEdit = {setIdToEdit}

                            />
                        )
                        
                    
                        })}        
                    </tbody>
                </Table>                
            </div>
            <button
                className = 'addBtnLocation'
                onClick = {() => setShowForm(!showForm)}
            >
                <AddLocationOutlinedIcon />
            </button>
            <LocationForm 
                handleSaveLocation = {handleSaveLocation}
                toggleForm = {toggleForm}
                showForm = {showForm}
                setShowForm = {setShowForm}
                locationToEdit = {locationToEdit}
                isEditing = {isEditing}
                setIsEditing = {setIsEditing}
            />
        </>
    )
}

export default Locations;