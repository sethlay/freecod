import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Card, CardBody, Label, Input } from 'reactstrap';
import './locationStyles/Locations.css';

const LocationForm = ({handleSaveLocation, showForm, setShowForm, isEditing, setIsEditing, locationToEdit}) => {

    const [location, setLocation] = useState('');


    return(
        <Modal
            className = 'locationModalForm'
            isOpen = {showForm} 
            toggle = {() => {
                setShowForm(false);
                setIsEditing(false);
            }}
        >
            <ModalHeader
                className = 'locationModalHeader'
                toggle = {() => {
                    setShowForm(false);
                    setIsEditing(false);
                }}  
            >
                {isEditing ? 'Edit Location' : 'Add Location'}
            </ModalHeader>
            <ModalBody>
                <Card>
                    <CardBody>
                        <Label
                            style ={{
                                color: 'midnightBlue'
                            }}
                        >Location:</Label>
                        <Input 
                            placeholder = 'Enter location'
                            type = 'text'
                            onChange = {(e) => setLocation(e.target.value)}
                            defaultValue = {isEditing ? locationToEdit.location : ''}
                        />
                    </CardBody>
                    <button
                        type = 'button'
                        disabled = {location === ''}
                        onClick = {() => {
                            handleSaveLocation(location);
                            setLocation('')
                        }}
                    >
                        {isEditing ? 'Update Location' : 'Save Location'}
                    </button>
                </Card>
            </ModalBody>

        </Modal>
    )
}

export default LocationForm;