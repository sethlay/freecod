import React from 'react';
// import {Button} from 'reactstrap';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditLocationIcon from '@material-ui/icons/EditLocation';

const LocationRow = ({location, handleDeleteLocation, setShowForm, setIsEditing, setLocationToEdit, setIdToEdit }) => {

    return(
        <tr>
            <td
                className = 'text-center'
                style = {{ color: 'darkBlue', width: '15vw'}}
            >{location.location}</td>
            <td>
                <button
                    onClick = {()=>handleDeleteLocation(location._id)}
                    className = 'locationDelete'
                >
                    <DeleteForeverIcon 
                        className = 'deleteForeveIcon'
                    />
                </button>
                <button
                    className = 'locationDelete'
                    onClick = { () => {
                        setShowForm(true);
                        setIsEditing(true);
                        setLocationToEdit(location);
                        setIdToEdit(location._id)
                    }}
                >
                    <EditLocationIcon 
                        className = 'deleteForeveIcon'
                    />
                </button>
            </td>
        </tr>
        
    )
}

export default LocationRow;