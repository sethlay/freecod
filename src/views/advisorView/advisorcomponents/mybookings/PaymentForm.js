import React from 'react';
import {CardElement, useStripe, useElements} from '@stripe/react-stripe-js';
import {Modal, ModalBody, ModalHeader, FormGroup, Button} from 'reactstrap';
import { SuccesPayToast, ErrorPayToast } from '../../../../mainComponents/Toasts';

const PaymentForm = ({setShowPaymentForm, showPaymentForm, updateScheduleBooking, scheduleId}) => {
   
  const stripe = useStripe();  
  const elements = useElements();
  


  const handleSubmit = async (event) => {
      // Block native form submission.
      event.preventDefault();
  
      if (!stripe || !elements) {
        // Stripe.js has not loaded yet. Make sure to disable
        // form submission until Stripe.js has loaded.
        return;
      }
  
      // Get a reference to a mounted CardElement. Elements knows how
      // to find your CardElement because there can only ever be one of
      // each type of element.
      const cardElement = elements.getElement(CardElement);
  
      // Use your card Element with other Stripe.js APIs
      
 
      const response = await fetch('https://quiet-reaches-06570.herokuapp.com/admin/secret/' + scheduleId);

      const {client_secret: clientSecret} = await response.json();

      stripe.confirmCardPayment(clientSecret, {
        payment_method: {
          card: cardElement,
          billing_details: {
            name: scheduleId
          }
        }
      })

      if(response.ok) {
        SuccesPayToast('Successfully Paid')
        updateScheduleBooking(scheduleId);
      }else{
        ErrorPayToast('An error occured. Please try again.')
      }
      
      setShowPaymentForm(false)
    
    };
  
    return (
        <Modal
            isOpen = {showPaymentForm}
            toggle = { () => setShowPaymentForm(false)}
        >
            <ModalHeader
                toggle = { () => setShowPaymentForm(false)}
            >
                Payment
            </ModalHeader>
            <ModalBody>              
                <form onSubmit={handleSubmit}>
                 <CardElement />
                  <button 
                    type="submit" 
                    disabled={!stripe}
                    className = 'm-3'
                  >Submit</button>
                </form>
            </ModalBody>
        </Modal>
      
    );
  };

export default PaymentForm;