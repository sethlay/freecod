import React, { useState, useEffect } from 'react';
import PictureAsPdfOutlinedIcon from '@material-ui/icons/PictureAsPdfOutlined';
import { Button } from 'reactstrap';
import './stylesBooking/MyBookings.css';
// import './stylesBooking/DownloadTicket.css';
import {Document, Page, View, StyleSheet, PDFDownloadLink, Text, Image} from '@react-pdf/renderer';
import moment from "moment"; 


const styles = StyleSheet.create({
    header: {
        height: '200px',
        width: '100%',
        textAlign: "center",
        backgroundColor: 'navajowhite',
        paddingTop: '15%',
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    brandName: {
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'Helvetica',
        color: 'midnightblue',
        textAlign: 'center'
    },
    detailsContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    details: {
        paddingTop: '10%',
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        paddingLeft: '5%',
        paddingRight: '25%'
    },
    detailText: {
        color: 'black',
        textAlign: 'center',
        margin: 5
        
    }
    
})

const Ticket = ({schedule}) => {
    return(
        <Document>
            <Page
                size = 'A4'
            >
                <View
                    // className = 'viewHeader'
                    style = {styles.header}
                >
                    <Text 
                        // className = 'textFontTitle' 
                        style = {styles.brandName}
                    >FREECOD</Text> 
                </View>
                <View
                    // className = 'viewContainer'
                    style = {styles.detailsContainer}
                >
                    <View
                        // className = 'viewDetails'
                        style = {styles.details}
                    >
                        <Text
                            // className = 'confirmationFont'
                            style = {styles.brandName}
                        >Confirmation Receipt</Text>
                        
                        <Text style = {styles.detailText}>
                            {schedule.advisor.name}
                        </Text>
                        <Text style = {styles.detailText}>
                            {schedule.advisor.credentials}
                        </Text>
                        <Text style = {styles.detailText}>
                            {moment(schedule.startDate).format('ll')} - {moment(schedule.endDate).format('ll')} 
                        </Text>
                        <Text style = {styles.detailText}>
                            Php {(schedule.amount) + '.00' }
                        </Text>
                        <Text style = {styles.detailText}>
                            {schedule.status} via {schedule.payment}
                        </Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

const DownloadTicket = ({schedule}) => {
      
    const [open, setOpen] = useState(false);

    useEffect(() => {
        setOpen(false);
        setOpen(true);
        return () => setOpen(false)
    })

    return(
        
        <Button
            className = 'bookingStyle'
        >
            
                {
                    open && <PDFDownloadLink
                        document = {<Ticket schedule = {schedule} />}
                        fileName = {'Ticket#' + Date.now() + '.pdf'}
                    >
                        { ( { loading } ) => loading ? 'Loading...' : <PictureAsPdfOutlinedIcon className = 'bookingIcon' /> }
                    </PDFDownloadLink>
                }
            
        </Button>
    )
}

export default DownloadTicket;