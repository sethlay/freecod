import React, { useState, useEffect } from 'react';
import MyBookingRow from '../mybookings/MyBookingRow';
import {Card, CardBody, Table} from 'reactstrap';
import './stylesBooking/MyBookings.css';
import {DateUtils} from "react-day-picker";



const MyBooking = () => {

    const [schedules, setSchedules] = useState([]);
        
    // console.log(schedules)   
    
    useEffect(() => {
        fetch('https://quiet-reaches-06570.herokuapp.com/admin/schedules/' + sessionStorage.userId)
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            setSchedules(res);
        })
    }, [])

    
    const updateScheduleBooking = id => {

        const newSchedules = schedules.map(indivSchedule => {
            if(indivSchedule._id === id) {
                indivSchedule.status = 'Paid';
                indivSchedule.payment = 'Stripe';
            }
            return indivSchedule;
        })
        setSchedules(newSchedules);
    }
    
    const cancelScheduleBooking = schedule => {
        
        const apiOptions = {
            method: 'PATCH',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                id: schedule._id,
                
            })
        }

        fetch('https://quiet-reaches-06570.herokuapp.com/admin/cancelschedule', apiOptions)
        .then(res => res.json())
        .then(res => {
            console.log(res)
            let newSchedules = schedules.map(indivSchedule => {
                if(indivSchedule._id === res._id) {
                    indivSchedule.status = res.status;
                    indivSchedule.payment = res.payment;
                }

                return indivSchedule;
            })
            setSchedules(newSchedules);
        })
    }
    
    

    return(
        <div
            className = 'bookingMainContainer'
        >
            <div
                className = 'bookingSubContainer'
            >
            <h5
                className = 'confirmFontStyle'
            >BOOKINGS</h5>
            <Card
                className = 'cardBookingStyle'
            >
                <CardBody
                    className = 'scrollContainer'
                >
                    <Table
                        className = 'text-center'
                    >
                        <thead>
                            <tr>
                                <th className = 'titleFont' ></th>
                                <th className = 'titleFont'>CONSULTANT</th>
                                <th className = 'titleFont'>SPECIALIZATION</th>
                                <th className = 'titleFont'>DATE</th>
                                <th className = 'titleFont'>AMOUNT</th>
                                <th className = 'titleFont'>STATUS</th>
                                <th className = 'titleFont'>REMARKS</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {schedules.map((schedule) => (
                                <MyBookingRow 
                                key = {schedule._id}
                                schedule = {schedule}
                                updateScheduleBooking = {updateScheduleBooking}
                                cancelScheduleBooking = {cancelScheduleBooking}
                                />
                            ))}
                        </tbody>
                    </Table>
                </CardBody>
            </Card>
            </div>
        </div>
    )
}

export default MyBooking;