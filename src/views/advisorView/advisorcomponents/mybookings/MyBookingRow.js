import React, { useState, useEffect } from 'react';
import './stylesBooking/MyBookings.css';
import moment from "moment";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import PaymentIcon from '@material-ui/icons/Payment';
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import PaymentForm from './PaymentForm';
import DownloadTicket from './DownloadTicket';

const stripePromise = loadStripe('pk_test_qMPCgEvJOHRpDrEd89MXRAED00l5bTzuAW');

const MyBookingRow = ({ schedule, updateScheduleBooking, cancelScheduleBooking }) => {
    
    const [showPaymentForm, setShowPaymentForm] = useState(false)

    
    return(
        <tr
            className = 'contentTextStyles'
        >
            <td>
                <img src = {'https://quiet-reaches-06570.herokuapp.com/' + schedule.advisor?.coverImg}
                style = {{
                    height: '6vh',
                    width: '4vw'
                }}/>
            </td>
            <td>{schedule.advisor.name} </td>
            <td>{schedule.advisor.credentials} </td>
            <td
                className = 'dateStyleRow'
            >{moment(schedule.startDate).format('ll')} - {moment(schedule.endDate).format('ll')}</td>
            <td>Php {(schedule.amount) + '.00'} </td>
            <td>{schedule.status} </td>
            <td>{schedule.payment} </td>
            <td>
            <button
                className = 'bookingStyle'
                disabled = {schedule.status === 'Pending' ? false : true}
                onClick = {() => cancelScheduleBooking(schedule)}
            >
                <HighlightOffIcon 
                    className = 'bookingIcon'
                />
            </button>
            <button
                className = 'bookingStyle'
                disabled = {schedule.status === 'Paid' || schedule.status === 'Cancelled'}
                onClick = {() => setShowPaymentForm(true)}
            >
                <PaymentIcon 
                    className = 'bookingIcon'
                />
            </button>
            {schedule.status === 'Paid' && <DownloadTicket schedule={schedule} />}
            <Elements
                stripe = {stripePromise}
            >
                <PaymentForm
                    setShowPaymentForm = {setShowPaymentForm}
                    showPaymentForm = {showPaymentForm}
                    amount = {schedule.amount}
                    scheduleId = {schedule._id} 
                    updateScheduleBooking = {updateScheduleBooking}
                />
            </Elements>
            </td>
        </tr>
    )
}

export default MyBookingRow;