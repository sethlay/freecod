import React from 'react'
import '../advisorStyles/Advisors.css';

const Hrs = () =>{
    return(
        <>
            <hr 
                className = 'bookingTitlehr'
            />
            <hr 
                className = 'verticalHrLeft'
            />
            <hr 
                className = 'verticalHrLeftMini'
            />
            <hr 
                className = 'verticalHrLeftMicro'
            />
            <hr 
                className = 'verticalHrRight'
            />
        </>
    )
}

export default Hrs;