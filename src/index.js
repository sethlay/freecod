import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';

// root main app
import MainViewApp from './MainViewApp';

// bootstrap
import 'bootstrap/dist/css/bootstrap.css'

// sidebar style
import '@trendmicro/react-sidenav/dist/react-sidenav.css';

import * as serviceWorker from './serviceWorker';

// import react-toastify styles
import 'react-toastify/dist/ReactToastify.css';
import {toast} from 'react-toastify';

toast.configure({
  autoClose: 5000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true
})

ReactDOM.render(
  // <React.StrictMode>
    // <FreecodApp />
    <MainViewApp />
    ,
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
