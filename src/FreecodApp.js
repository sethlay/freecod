import React from 'react';
import './landingpagestyles/FreecodApp.css';
import LandingTextAccess from './landingpagecomponents/LandingTextAccess';
import LandingPageInfo from './landingpagecomponents/LandingPageInfo';


const FreecodApp = () =>{

    return(
        <>
            <div
                className='containerMain'
            ></div>
            <div
                className='mainDiv'
            > 
                <div
                    className='topContent'
                >
                    <h2
                        className='topText'
                        style={{
                            margin:'0'
                        }}
                    >"Hope is not gone"</h2>
                    <h2
                        className='topNextText'
                        style={{
                            margin:'0'
                        }}
                    >The best way out is always through.</h2>
                    <div
                        className = 'spinningGlobe'
                    ></div>
                    <div
                        className = 'peaceGifStyle'
                    >

                    </div>
                    
                    <div
                        className = 'brandStyle'
                    >FreeCOD</div>

                </div>
                <hr className = 'hrBrand' />
                <div
                    className='subContentHollow'
                >
                    
                </div>

                <div
                    className='landingPageTextAccess'
                >
                    <LandingTextAccess />
                </div>
                
                <div
                    className='landingPageYoutube'
                >
                    <LandingPageInfo />
                </div>

            </div>
                   
        </>
    )
}

export default FreecodApp;